FROM node:alpine

RUN mkdir -p /data
COPY ./dist /data
COPY ./views /data/views
COPY ./package*.json /data/
WORKDIR /data
RUN npm ci 

EXPOSE 8080:80
ENV PORT 80
ENV EXTERNAL_OWN_URL http://localhost:8080
ENTRYPOINT [ "node", "index.js" ]