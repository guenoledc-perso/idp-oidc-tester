import express, {
  Request,
  Response,
  NextFunction,
  RequestHandler
} from "express";
import {
  Issuer,
  Client,
  TokenSet,
  IntrospectionResponse,
  UserinfoResponse,
  OpenIDCallbackChecks,
  IdTokenClaims
} from "openid-client";
import logger from "../logger";
import { CacheInterface } from "./adapter-interfaces";
import uuid from "uuid";
import JWT from "jsonwebtoken";

const CookiePrefix = process.env.COOKIE_PREFIX || "OIDC";
const SessionCookieName = CookiePrefix + "SecureToken";
const UserEmailCookieName = CookiePrefix + "Email";
const ServiceListCookieName = CookiePrefix + "Services";
const CSRFTokenCookieName = "CSRF-Token";
const SessionDetailsPrefix = "SessionDetails-";
const SessionCheckKeyPrefix = "SessionCheck-";

export class InvalidSession extends Error {
  constructor(message: string) {
    super(message);
  }
}

export class NotFound extends Error {
  constructor(message: string) {
    super(message);
  }
}

export class AlreadyExist extends Error {
  constructor(message: string) {
    super(message);
  }
}

export type SessionMwOps = {
  failOnMissing?: boolean;
  loadUserInfo?: boolean;
  loadIntrospect?: boolean;
};
const defaultSessionMwOps: SessionMwOps = {
  failOnMissing: false,
  loadIntrospect: false,
  loadUserInfo: false
};

type SessionCheckData = {
  checks: OpenIDCallbackChecks;
  redirectUri: string;
};

type SessionDetails = {
  tokens: TokenSet;
  expiresAt?: number;
  idTokenDecoded?: IdTokenClaims;
  email?: string;
  services?: string[];
  jti?: string;
  config?: string;
};

function baseUrl(req: Request): string {
  return (
    process.env.EXTERNAL_OWN_URL || req.protocol + "://" + req.headers.host
  );
  //return req.protocol + "://" + req.headers.host;
}
const defaultSessionDurationMs : number = 1000*60*60*8; // 8h
function setSessionCookies(res: Response, details: SessionDetails): void {
  const tokens = details.tokens;
  const now = Date.now();
  const expiresAtMs = tokens.expires_at ? tokens.expires_at * 1000 : (now+defaultSessionDurationMs);
  const durationMs: number = expiresAtMs - now + 5000;
  //console.log("SET SESSION COOKIES", durationMs, new Date(expiresAtMs))

  if (!details.services) {
    details.services = [];
  } else if (!Array.isArray(details.services)) {
    details.services = [(details.services as any).toString()];
  }
  if (!details.services.includes("api")) {
    details.services.push("api");
  }
  if (!details.services.includes("session")) {
    details.services.push("session");
  }
  res.cookie(ServiceListCookieName, details.services.join(":"), {
    httpOnly: true,
    maxAge: durationMs,
    path: "/session"
  });

  details.services.forEach(svc => {
    res.cookie(SessionCookieName, tokens.access_token, {
      httpOnly: true,
      maxAge: durationMs,
      path: "/" + svc.toLowerCase()
    });
  });

  // TODO: need to be removed and replaced by path only cookie ?
  // res.cookie(SessionCookieName, tokens.access_token, {
  //   httpOnly: true,
  //   maxAge: durationMs,
  //   path: "/"
  // });

  if (details.email) {
    res.cookie(UserEmailCookieName, details.email, {
      path: "/",
      httpOnly: true,
      // no expiry on this one
      maxAge: 1000 * 60 * 60 * 24 * 360 * 10 // 10 years
    });
  }

  if (!details.jti) {
    res.cookie(CSRFTokenCookieName, details.jti, {
      path: "/",
      httpOnly: false,
      maxAge: durationMs
    });
  }
}

function clearSessionCookies(req: Request, res: Response): void {
  let serviceList: string[] = [];
  try {
    serviceList = req.cookies[ServiceListCookieName].split(":");
  } catch (error) {
    logger.error("Invalid cookie format, expected : separated strings", {
      name: ServiceListCookieName,
      value: req.cookies[ServiceListCookieName]
    });
  }
  res.clearCookie(ServiceListCookieName, {
    path: "/session"
  });

  serviceList.forEach(svc => {
    res.clearCookie(SessionCookieName, {
      path: "/" + svc.toLowerCase()
    });
  });

  res.clearCookie(SessionCookieName, {
    path: "/"
  });

  res.clearCookie(CSRFTokenCookieName, {
    path: "/"
  });
}

export type ServicesGetter = (instrospectResponse: any) => Promise<string[]>;
export type OIDCClientGetter = (req: Request) => Promise<Client | undefined>;

export class SessionMgmt {
  private oidcClientGetter: (req: Request) => Promise<Client>;;
  private cache: CacheInterface;
  private acceptedRedirectUris: string[];
  private getServices: ServicesGetter;

  constructor(
    oidcClientGetter: OIDCClientGetter,
    cache: CacheInterface,
    servicesGetter?: ServicesGetter
  ) {
    this.oidcClientGetter = async (req: Request)=> {
      const client = await oidcClientGetter(req);
      if(client) {
        return client
      } 
      throw new Error("Configuration is not defined")
    }
    this.cache = cache;
    if (servicesGetter) {
      this.getServices = servicesGetter;
    } else {
      this.getServices = (ir: any) => Promise.resolve([]);
    }

    if (process.env.ACCEPTED_REDIRECT_URIS) {
      this.acceptedRedirectUris = process.env.ACCEPTED_REDIRECT_URIS.split(";");
    } else {
      if (!process.env.EXTERNAL_OWN_URL) {
        throw new Error(
          "Expected definition of EXTERNAL_OWN_URL env var to default ACCEPTED_REDIRECT_URIS"
        );
      } else {
        this.acceptedRedirectUris = [process.env.EXTERNAL_OWN_URL+"*"];
      }
    }
  }

  private getAccessToken(req: Request): string | undefined {
    const token: string = req.cookies[SessionCookieName];
    if (token) {
      return token;
    } else {
      return undefined;
    }
  }

  private validateRedirectUri(redirectUri: string): boolean {
    const foundAt = this.acceptedRedirectUris.findIndex(uri => {
      if (uri.toLowerCase().startsWith("http")) {
        if (uri.endsWith("*")) {
          return redirectUri.startsWith(uri.substr(0, uri.length - 1));
        } else {
          return uri == redirectUri;
        }
      } else {
        try {
          const re = new RegExp(uri);
          return re.test(redirectUri);
        } catch (error) {
          logger.warn("Invalid RedEx string", {
            message: error.message,
            uri: uri
          });
          return false;
        }
      }
    });
    // found if index not -1
    return foundAt >= 0;
  }

  private async clearSessionCache(
    accessToken: string | undefined
  ): Promise<boolean> {
    if (accessToken) {
      //console.log("CLEAR SESSION CACHE", SessionDetailsPrefix + accessToken)

      return await this.cache.del(SessionDetailsPrefix + accessToken);
    } else {
      return false;
    }
  }

  private async saveSessionCache(
    details: SessionDetails,
    previousAccessToken?: string
  ): Promise<boolean> {
    await this.clearSessionCache(previousAccessToken);

    if (!details.expiresAt) {
      details.expiresAt = (details.tokens.expires_at || (Date.now()+defaultSessionDurationMs)/1000) * 1000;
    }
    const duration = Math.round((details.expiresAt - Date.now()) / 1000);

    if (details.tokens.access_token) {
      //console.log("SAVE SESSION CACHE", SessionDetailsPrefix + details.tokens.access_token, duration, new Date(details.expiresAt))
      return await this.cache.put(
        SessionDetailsPrefix + details.tokens.access_token,
        { ...details }, // force copying the data
        duration
      );
    } else {
      return false;
    }
  }

  private async updateDetailsFromIDToken(accessToken: string, details: SessionDetails): Promise<void> {
    const idToken = details.tokens.id_token
    if (!idToken) {
      throw new InvalidSession("Missing id_token in session")
    }

//    logger.debug("idToken: ", { idToken })

    details.idTokenDecoded = JWT.decode(idToken, { json: true }) as IdTokenClaims
    
    if (!details.idTokenDecoded.authorizations) {
      details.idTokenDecoded.authorizations = []
    }

    details.services = await this.getServices(details.idTokenDecoded)
    details.email = details.idTokenDecoded.email as string
    details.jti = (details.idTokenDecoded.jti as string) || "missing-jti-" + uuid()
  }

  private async getSessionDetails(
    oidcCient: Client,
    accessToken: string,
    needIntrospect = false
  ): Promise<SessionDetails> {
    const details: SessionDetails | undefined = await this.cache.get(
      SessionDetailsPrefix + accessToken
    );
    
    if (!details || !details.tokens) {
      logger.error("Missing a session", { sessionDetails: details });
      throw new InvalidSession("Missing a session");
    }
    if (!details.idTokenDecoded && needIntrospect) {
      await this.updateDetailsFromIDToken(accessToken, details)
      details.config = oidcCient.config as string;
      await this.saveSessionCache(details)
    }
//console.log("getSessionDetails:", JSON.stringify(details, null, 2))
    return details;
  }

  private async saveSessionCheckInfo(
    key?: string,
    data?: SessionCheckData
  ): Promise<boolean> {
    if (key && data) {
      return await this.cache.put(SessionCheckKeyPrefix + key, data, 10 * 60);
    } else {
      return false;
    }
  }
  getRedirectUri(req: Request): string {
    let redirectUri =
      req.query.redirect_uri || req.headers.referer || baseUrl(req);
    if (redirectUri.startsWith("/")) {
      redirectUri = baseUrl(req) + redirectUri;
    }
    return redirectUri;
  }

  getRegisterMw(): RequestHandler {
    return this.getLoginMw(true);
  }
  getLoginMw(register = false): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      const redirectUri = this.getRedirectUri(req);

      if (!this.validateRedirectUri(redirectUri)) {
        return next(
          "The requested redirectUri is not accepted: " + redirectUri
        );
      }

      try {
        const oidcClient = await this.oidcClientGetter(req);
        
        // start by clearing an existing session if any
        this.clearSessionCache(this.getAccessToken(req));
        // clear the cookies anyway before starting the authentication process
        clearSessionCookies(req, res);

        const checks: OpenIDCallbackChecks = {
          nonce: "N-" + Math.random(),
          state: uuid()
        };
        await this.saveSessionCheckInfo(checks.state, {
          checks,
          redirectUri: redirectUri
        });
        let uri = oidcClient.authorizationUrl({
          nonce: checks.nonce,
          state: checks.state,
          login_hint: req.query.email || req.cookies[UserEmailCookieName],
          prompt: req.query.prompt,
          scope: oidcClient.metadata.default_scope as string || "openid"
        });
        if (register) {
          uri = uri.replace("/auth?", "/registrations?");
        }
        logger.debug("Authentication with url", { url: uri });
        res.redirect(uri);
        next();
      } catch (error) {
        logger.error("Failed trying to initiate a login", {
          message: error.message,
          fullError: error
        });
        next(error);
      }
    };
  }

  getLogoutMw(): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      const token = this.getAccessToken(req);
      await this.clearSessionCache(token);
      try {
        res.redirect((await this.oidcClientGetter(req)).endSessionUrl());
      } catch (error) {
        return this.getPostLogoutMw()(req,res,next);
      }
      // cookies are cleared on PostLogout
      next();
    };
  }

  getPostLogoutMw(): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      clearSessionCookies(req, res);
      res.redirect(process.env.POST_LOGOUT_URL || "/"); // TODO define a landing page post logout
      next();
    };
  }
  getUserInfoMw(): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      try {
        const token = this.getAccessToken(req);
        if (token) {
          try {
            const userInfo = await (await this.oidcClientGetter(req)).userinfo(token);
            res.json(userInfo);
          } catch (error) {
            res.json({
              error: "fail getting user info",
              details: error
            })
          }
        } else {
          res.json({ // empty result
          });
        }
        next();
      } catch (error) {
        logger.error("Fails getting all tokens", {
          message: error.message,
          fullError: error
        });
        next(error);
      }
    }
  }
  getTokensMw(): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      try {
        const token = this.getAccessToken(req);
        const oidcClient = await this.oidcClientGetter(req);
        if (token) {
          const details = await this.getSessionDetails(oidcClient, token, true);
          let decodedIdToken: any;
          let decodedAccessToken: any;
          let decodedRefreshToken: any;
          try {
            decodedIdToken = JWT.decode(details.tokens.id_token as string, {complete: true, json: true})
          } catch (error) {
            decodedIdToken = {
              error: "Fail to parse the ID Token",
              details: error
            }
          }
          try {
            decodedAccessToken = await oidcClient.introspect(details.tokens.access_token as string);
          } catch (error) {
            console.error("Fail to call introspect endpoint for access token", error)
            decodedAccessToken = {
              error: "Fail to call introspect endpoint for access token",
              details: error.message || error.toString()
            }
          }
          try {
            if(details.tokens.refresh_token) 
              decodedRefreshToken = await oidcClient.introspect(details.tokens.refresh_token as string);
            else 
              decodedRefreshToken = undefined
          } catch (error) {
            decodedRefreshToken = {
              error: "Fail to call introspect endpoint for refresh token",
              details: error.message || error.toString()
            }
          }
          res.json({
            accessToken: details.tokens.access_token,
            idToken: details.tokens.id_token,
            refreshToken: details.tokens.refresh_token,
            decoded: {
              accessToken: decodedAccessToken,
              idToken: decodedIdToken,
              refreshToken: decodedRefreshToken
            }
          });
        } else {
          res.json({ // empty result
          });
        }
        next();
      } catch (error) {
        logger.error("Fails getting all tokens", {
          message: error.message,
          fullError: error
        });
        next(error);
      }
    }
  }
  getStateMw(): RequestHandler {
    return async (
      req: Request,
      res: Response,
      next: NextFunction
    ): Promise<void> => {
      try {
        const token = this.getAccessToken(req);
        const oidcClient = await this.oidcClientGetter(req);
        if (token) {
          const details = await this.getSessionDetails(oidcClient, token, true);
          const expiresAt = details.expiresAt || Date.now()+defaultSessionDurationMs;
          let expiresInMs = expiresAt - Date.now();
          if (expiresInMs < 0) {
            expiresInMs = 0;
          }
          res.json({
            config: details.config,
            active: true,
            ...details,
            expiresAt: expiresAt,
            expiresInMs: expiresInMs,
            cookies: req.cookies
          });
        } else {
          res.json({
            active: false,
            email: req.cookies[UserEmailCookieName]
          });
        }
        next();
      } catch (error) {
        logger.error("Fails getting session state", {
          message: error.message,
          fullError: error
        });
        next(error);
      }
    };
  }

  getCodeAuthFlowCallbackMw(): RequestHandler {
    return async (req: Request, res: Response, next: NextFunction) => {
      try {
        const oidcClient = await this.oidcClientGetter(req);
        // get and clear the one time check data for the callback
        const data: SessionCheckData = await this.cache.get(
          SessionCheckKeyPrefix + req.query.state
        );
        await this.cache.del(SessionCheckKeyPrefix + req.query.state);

        if (!data) {
          // when the login process took too long to process the SesionCheckData is lost in the cache so login must be restarted
          logger.info("Session check data expired so retry to login");
          return this.getLoginMw(false)(req, res, next);
        }

        logger.debug("Callback params:", {
          redirectUri: baseUrl(req) + req.baseUrl + req.path,
          params: (await this.oidcClientGetter(req)).callbackParams(req),
          checks: data.checks
        });
        // retrieve the session from code
        const tokens: TokenSet = await oidcClient.callback(
          baseUrl(req) + req.baseUrl + req.path,
          oidcClient.callbackParams(req),
          data.checks
        );
        await this.saveSessionCache({ tokens }, this.getAccessToken(req));
        // set the cookies
        if (tokens.access_token) {
          const details = await this.getSessionDetails(
            oidcClient,
            tokens.access_token,
            true
          );
          setSessionCookies(res, details);
        }

        res.redirect(data.redirectUri);
      } catch (error) {
        console.error(error)
        logger.error("Fails getting tokens", {
          message: error.message,
          fullError: error
        });
        next(error);
      }
      next();
    };
  }

  private async refreshSession(
    oidcClient: Client,
    accessToken: string
  ): Promise<TokenSet | undefined> {
    const details = await this.getSessionDetails(oidcClient, accessToken, true);
    if (details.tokens.refresh_token) {
      const tokens = await oidcClient.refresh(
        details.tokens.refresh_token
      );
      logger.debug("Refresh tokens:", tokens)
      if(!tokens.refresh_token) {
        // set the initial refresh token
        tokens.refresh_token = details.tokens.refresh_token
      }
      await this.saveSessionCache({ tokens }, accessToken);
      return tokens;
    } else {
      return undefined;
    }
  }

  getRefreshSessionMw(): RequestHandler {
    return async (req: Request, res: Response, next: NextFunction) => {
      const accessToken = this.getAccessToken(req);
      if (!accessToken) {
        res.status(401);
        return next();
      }
      try {
        const oidcClient = await this.oidcClientGetter(req);
        const tokens = await this.refreshSession(oidcClient, accessToken);
        if (tokens) {
          if (tokens.access_token) {
            const details = await this.getSessionDetails(
              oidcClient,
              tokens.access_token,
              true
            );
            setSessionCookies(res, details);
            res.status(200);
          } else {
            this.clearSessionCache(accessToken);
            clearSessionCookies(req, res);
            logger.error("Failed refreshing the session because no access token has been returned", {
              tokens
            });
            res.status(500);
          }
        } else {
          this.clearSessionCache(accessToken);
          clearSessionCookies(req, res);
          res.status(403);
        }
      } catch (error) {
        logger.error("Failed refreshing the session", {
          message: error.message,
          fullError: error
        });
        this.clearSessionCache(accessToken);
        clearSessionCookies(req, res);
        res.status(500);
      }
      next();
    };
  }

  getSessionMw(options: SessionMwOps): RequestHandler {
    return async (req: Request, res: Response, next: NextFunction) => {
      const opts: SessionMwOps = {
        ...defaultSessionMwOps,
        ...options
      };
      const token = this.getAccessToken(req);
      const oidcClient = await this.oidcClientGetter(req);
      if (opts.failOnMissing && !token) {
        return next(new InvalidSession("Invalid session"));
      }
      try {
        if (token) {
          res.locals.session = {
            accessToken: token
          };
          if (opts.loadUserInfo) {
            res.locals.session.userInfo = await oidcClient.userinfo(token);
          }
          if (opts.loadIntrospect) {
            const details = await this.getSessionDetails(oidcClient, token, true);
            res.locals.session.introspect = details.idTokenDecoded;
          }
        }
        next();
      } catch (error) {
        if (opts.failOnMissing) {
          next(error);
        } else {
          res.locals.session = undefined;
          next();
        }
      }
    };
  }

  getEmailFromCookie(req: Request): string | undefined {
    if (req.cookies[UserEmailCookieName]) {
      return req.cookies[UserEmailCookieName];
    } else {
      return undefined;
    }
  }

  /* eslint-disable @typescript-eslint/camelcase */
  private async getRegistrationUrl(oidcClient: Client, redirectUri: string): Promise<string> {
    return oidcClient
      .authorizationUrl({
        redirect_uri: redirectUri
      })
      .replace("/auth?", "/registrations?");
  }
}
